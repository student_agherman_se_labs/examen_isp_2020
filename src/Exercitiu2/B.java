package Exercitiu2;

public class B extends A {
    //inheritance B is a class A
    private String param;
    private C c; //composition owns C When B is deleted, C is deleted
    public void myMethod(Z z){z.g();} //dependency uses an instance of class Z
}

class A
{

}
class C
{

}
class E
{
    B b; //aggregation has B
}
class D
{

    public void f(B bb) {}  //association uses B


}
class Z
{
    public void g(){}
}

