package Exercitiu1;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Threads  extends Thread{

        Threads(String name){super(name);}
    public void run(){
        for(int i=0;i<5;i++){
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
            System.out.println(getName() + " - Current Time "+formatter.format(date));
            try {
                Thread.sleep((int)(5000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " job finalised.");
    }
    public static void main(String[] args) {
        Threads StrThread1 = new Threads("StrThread1");
        Threads StrThread2 = new Threads("StrThread2");

        StrThread1.run();
        StrThread2.run();

    }
}

